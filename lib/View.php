<?php
namespace lib;

class View
{
	public $path;
	public $route;
	public $layout = 'layout'; 

	function __construct($route)
	{
		$this->route = $route;
		$this->path = 'resources/view/'.$route['action'];
	}

	public function render($title, $vars = [])
	{
		ob_start();
		require $this->path.'.php';
		$content = ob_get_clean();
		require 'resources/'.$this->layout.'.php';
	}
}