<?php
namespace lib;

class Route
{
	protected $routes = [];
	protected $params = [];

	function __construct()
	{
		$arr = require 'routes.php';
		foreach($arr as $key => $val)
		{
			$this->add($key, $val);
		}
	}

	public function add($route, $params)
	{
		$route = '#^'.$route.'$#';
		$this->routes[$route] = $params; 
	}
	
	public function match()
	{
		$url = ltrim($_SERVER['REQUEST_URI'],'/index.php');
		foreach($this->routes as $route => $params)
		{
			if(preg_match($route, $url, $matches))
			{
				$this->params = $params;
				return true;
			}
		}
		return false;
	}

	public function run()
	{
		if($this->match())
		{
			$path = 'controllers\\'.ucfirst($this->params['controller']).'Controller';
			if(class_exists($path))
			{
				$action = $this->params['action'];
				if(method_exists($path, $action))
				{
					$controller = new $path($this->params);
					$controller->$action();
				}else{
					echo 'Экшен не найден';
				}
			}else{
				echo 'Не найден контроллер '.$path;
			}
		}
	}
}