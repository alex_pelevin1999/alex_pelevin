<!DOCTYPE html>
<html>
<head>
	<title><?php echo $title?></title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="/css/css.css">
</head>
<body>
	<header>
		<nav class="navbar navbar-expand-lg navbar-light bg-light justify-content-between">
		  <a class="navbar-brand" href="#">Test-taska</a>
		  <ul class="navbar-nav navbar-right">
		      <li class="nav-item active">
		        <a class="nav-link" href="/index.php/login">Autorisation <span class="sr-only">(current)</span></a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="/">Registration</a>
		      </li>
		    </ul>
		</nav>
	</header>

	<?php
		echo $content;
	?>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
	<?php
		if($title == "Registration")
		{
	?>
			<script type="text/javascript" src="js/regist.js"></script>
	<?php
		}else if($title == "Autorisation")
		{
	?>
			<script type="text/javascript" src="/js/login.js"></script>
	<?php
		}
	?>
	
</body>
</html>