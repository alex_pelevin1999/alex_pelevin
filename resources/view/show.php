<?php
	if(!isset($_SESSION['name']) && !isset($_SESSION['name']))
	{
?>
	<h1>You are not logged in.</h1>
<?php
	}else{
?>	
	<div class="jumbotron">
	  <h1 class="display-4">Hello, <?php echo $_SESSION['name'] ?></h1>
	  <p class="lead">It is your email - <?php echo $_SESSION['email']?></p>
	  <hr class="my-4">
	  <p class="lead">
	    <a class="btn btn-primary btn-lg" href="/index.php/login" role="button">Exit</a>
	  </p>
	</div>
<?php		
	}
?>
