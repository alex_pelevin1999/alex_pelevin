<div class="regist">
	<form id='target'>
	  <div class="form-group">
	    <label for="InputEmail">Email address</label>
	    <input type="email" class="form-control" name="email" id="InputEmail" required="required" placeholder="Enter email">
	    <small id="emailHelp" class="form-text ">Email isn't unique!</small>
	  </div>
	  <div class="form-group">
	    <label for="InputLogin">Login</label>
	    <input type="text" class="form-control" name="login" id="InputLogin" required="required"  placeholder="Enter login">
	    <small id="loginHelp" class="form-text ">Login isn't unique!</small>
	  </div>
	  <div class="form-group">
	    <label for="InputName">Real Name</label>
	    <input type="text" class="form-control" name="name" id="InputName" required="required" placeholder="Enter name">
	    <small id="namelHelp" class="form-text">Invalid name specified! Example: Ivanov Ivan.</small>
	  </div>
	  <div class="form-group">
	    <label for="InputPassword">Password</label>
	    <input type="password" class="form-control" name="pass" id="InputPassword" required="required" placeholder="Password">
	    <small id="passHelp" class="form-text">The password must include only numbers and letters of the Latin alphabet, at least 6 symbols!</small>
	  </div>
	  <div class="form-group">
	    <label for=SelectCountry">Select country</label>
	    <select class="form-control" name="country" id="SelectCountry">
	      <option>Country</option>
	      <?php
	     		foreach($vars as $key)
	     		{
	     	?>
	     			<option><?php echo $key['name'] ?></option>
	     	<?php
	     		}
	     	?>
	    </select>
	    <small id="countryHelp" class="form-text ">Select your country!</small>
	  </div>
	  <div class="form-group">
	    <label for="SelectDay">Your date of birth</label>
	    <select class="form-control favor_day" name="day" id="SelectDay">
	      	<option>Day</option>
	     	<?php
	     		for($i=1; $i<=31; $i++)
	     		{
	     	?>
	     			<option><?php echo $i ?></option>
	     	<?php
	     		}
	     	?>
	    </select>
	    <select class="form-control favor_day" name="month" id="SelectMonth">
	      	<option>Month</option>
	      	<option>January</option>
	      	<option>February</option>
	      	<option>March</option>
	      	<option>April</option>
	      	<option>May</option>
	      	<option>June</option>
	      	<option>July</option>
	      	<option>August</option>
	      	<option>September</option>
	      	<option>October</option>
	      	<option>November</option>
	      	<option>December</option>
	    </select>
	    <select class="form-control favor_day" name="year" id="SelectYear">
	      	<option>Year</option>
	      	<?php
	     		for($i=1980; $i<=2019; $i++)
	     		{
	     	?>
	     			<option><?php echo $i ?></option>
	     	<?php
	     		}
	     	?>
	    </select>
	    <small id="dateHelp" class="form-text "></small>
	  </div>
	  <div class="form-check">
	    <input type="checkbox" id="agree" name="check" class="form-check-input" id="exampleCheck1">
	    <label class="form-check-label" for="exampleCheck1">Give your consent to the data processing.</label>
	  </div>
	  <button id="btn-submit" class="btn btn-primary">Submit</button>
	</form>
</div>