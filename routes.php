<?php

return[
	'' => [
		'controller' => 'account',
		'action' => 'register',
	],

	'login' => [
		'controller' => 'account',
		'action' => 'login',
	],

	'show' => [
		'controller' => 'account',
		'action' => 'show',
	],

	'check' => [
		'controller' => 'account',
		'action' => 'check',
	],

	'autor' => [
		'controller' => 'account',
		'action' => 'autor',
	]
];