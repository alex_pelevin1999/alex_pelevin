<?php
namespace database;


class DB
{

	public $dbhost;
    public $dbuser;
    public $dbpass;
    public $db_name;
	public $conn;
	
	function __construct()
	{
		$config = require 'config.php';
		$this->dbhost = $config['host'];
		$this->dbuser = $config['user'];
		$this->dbpass = $config['password'];
		$this->db_name = $config['db_name'];
		$this->conn = mysqli_connect($this->dbhost, $this->dbuser, $this->dbpass, $this->db_name);
	}

	public function query_insert($query)
	{
		if(!$this->conn)
		{
			mysqli_error();
			mysqli_close($this->conn);
			exit();
		}else{
			$result = mysqli_query($this->conn,$query);
			mysqli_close($this->conn);
			return $result;
		}
	}
	public function query_select($query)
	{
		if(!$this->conn)
		{
			mysqli_error();
			mysqli_close($this->conn);
			exit();
		}else{
			$result = mysqli_query($this->conn, $query);
			if (mysqli_num_rows($result) > 0)
			{
				$i = 0;
				while($row = mysqli_fetch_assoc($result))
				{
					$res[$i] = $row;
					$i++;
				}
			}
			mysqli_close($this->conn);
			return $res;
		}
	}
	public function db_check($query)
	{
		$result = mysqli_num_rows(mysqli_query($this->conn, $query));
		return $result;
	}
}