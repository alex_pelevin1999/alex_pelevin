var myEpoch = 0;

		function checking(name,pass,day,month,year){
			let result;

    		if(!$("#agree").is(':checked'))
    		{
    			$(".form-check-label").css("color","red");
    			result = false; 
    		}else{$(".form-check-label").css("color","black");}

    		if(!name.match(/^[A-Z]{1}[a-z]{2,10}\s[A-Z]{1}[a-z]{2,10}\s{0,}$/))
    		{
    			$("#namelHelp").css("display","block");
    			result = false; 
    		}else{$("#namelHelp").css("display","none");}

    		if(!pass.match(/^[0-9A-Za-z]{6,}$/))
    		{
    			$("#passHelp").css("display","block");
    			result = false; 
    		}else{$("#passHelp").css("display","none");}

	    	if(day == 'Day' || month == 'Month' || year == "Year"){
	    		$("#dateHelp").text("Select birthday!").css("display","block");
	    		result = false;
	    	}else if(day > 29  && month == 'February'){
	    		$("#dateHelp").text("There is no such day this month!").css("display","block");
	    		result = false;
	    	}else if(day > 30  && (month == 'April ' || month == 'June' || month == 'September' || month == 'November')){
	    		$("#dateHelp").text("There is no such day this month!").css("display","block");
	    		result = false;
	    	}else{
	    		var time = month+" "+day+", "+year;
		        var myDate = new Date(time+" 04:30:00"); // Your timezone!
				myEpoch = myDate.getTime()/1000.0;
				$("#dateHelp").css("display","none");
	    		if(result != false)
	    			{
	    				result = true;
	    			}
	    	}
	    	return result;
		}	

		$('#target').submit(function(e){
			e.preventDefault();
				
		        var email = $("input[name=email]").val();
		        var login = $("input[name=login]").val();
		        var name = $("input[name=name]").val();
		        var password = $("input[name=pass]").val();
		        var country = $("select[name=country]").val();
		        var check = $("input[name=check]").val();

		        var day = $("select[name=day]").val();
		        var month = $("select[name=month]").val();
		        var year = $("select[name=year]").val();
		        
		    if(checking(name,password,day,month,year))
			{
		        $.ajax({
		           type:'POST',
		           url:'index.php/check',
		           data:{email:email, login:login, name:name, password:password, country:country, date:myEpoch},
		           success:function(data){
		           	$("input[name=email]").val(email).css('border', "1px solid #ced4da");
		           	$("input[name=login]").val(login).css('border', "1px solid #ced4da");
		           	$("#emailHelp").css("display","none");
		           	$("#loginHelp").css("display","none");
		           		if(data)
		           		{
			           		var some = $.parseJSON(data);
		           			if(some.wr_email || some.wr_login)
			           		{
				           		if(some.wr_email)
				           		{
				           			$("input[name=email]").val(email).css('border', "1px solid red");
				           			$("#emailHelp").css("display","block");
				           			$("input[name=login]").val(login);
				           		}	
				           		if(some.wr_login)
							    {
							    	$("input[name=email]").val(email);
							    	$("#loginHelp").css("display","block");
				           			$("input[name=login]").val(login).css('border', "1px solid red");
							    }
							    $("input[name=name]").val(name);
							    $("input[name=pass]").val('');
							    $("select[name=country]").val(country);
							    $("select[name=day]").val(day);
							    $("select[name=month]").val(month);
							    $("select[name=year]").val(year);
							    $("input[name=check]").val(check);    
							}
		           		}else{
							window.location.href = 'index.php/show';
						}
			        }
		        });
			}
		});